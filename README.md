# StreamerSonglist Translations

## Current Languages

- cs - Czech
- de - German
- en - English
- es - Spanish
- fr - French
- ja - Japanese
- ru - Russian
- zh-CN - Chinese (Simplified)

